# TICK 
TICK 是由 InfluxData 开发的一套运维工具栈，由 Telegraf, InfluxDB, Chronograf, Kapacitor 四个工具的首字母组成。
这一套组件将收集数据和入库、数据库存储、展示、告警四者囊括了。

- Telegraf：<br>
是一个数据收集和入库的工具。提供了很多 input 和 output 插件，比如收集本地的 cpu、load、网络流量等数据，然后写入 InfluxDB 、Kafka或者- OpenTSDB等。相当于ELK栈中的 logstash 功能。
- InfluxDB：<br>
是一个开源的GO语言为基础的数据库, 用来处理时间序列数据,提供了较高的可用性。与opentsdb类似，支持HTTP API方式，写入和读取数据。相当于ELK栈中的elasticsearch功能。
- Chronograf：<br>
从InfluxDB时间序列数据的数据可视化工具，负责从InfluxDB收集数据，并将数据图表以web的形式发布。它使用简单,包括模板和库可以快速构建实时数据的可视化仪表板，轻松地创建报警和自动化的规则。相当于ELK栈中的kibana功能。
- Kapacitor：<br>
是InfluxDB的数据处理引擎，主要作用是时间序列数据处理、监视和警报。

# 一、准备环境
## 1、配置Kubelet服务
首先Kubernetes要开启kubelet的10255端口，telegraf要通过这个端口进行数据采集。使用下面命令将readOnlyPort参数加入到各节的的/var/lib/kubelet/config.yaml 文件中：
```shell
sed -i '$a\readOnlyPort: 10255' /var/lib/kubelet/config.yaml
systemctl restart kubelet
```
## 2、安装Helm 3.4.2
```shell
# 下载helm二进制文件
curl -O https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz
tar -zxvf helm-v3.4.2-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
# 添加helm仓库
helm repo add apphub https://apphub.aliyuncs.com
helm repo add elastic https://helm.elastic.co
```
## 3、创建持久化存储卷
准备NFS存储服务器一台
|    IP地址   |   port   |   服务   |
|-------------|:--------:|:--------:|
|192.168.1.100| 111,2029 |NFS Server|
```shell
# 安装nfs组件
yum install nfs-utils rpcbind

# 安装 nfs-client-provisioner
helm install nfs-storage -n default \
--set nfs.server=192.168.0.100,nfs.path=/data/NFS \
--set storageClass.name=nfs-storage,storageClass.reclaimPolicy=Delete \
apphub/nfs-client-provisioner
```
- nfs.server：指定 nfs 服务地址
- nfs.path：指定 nfs 对应目录
- storageClass.name：此配置用于绑定 PVC 和 PV
- storageClass.reclaimPolicy：回收策略，默认是删除；保留改为Retain
- -n：命名空间
## 4、下载项目代码
```
git clone https://gitlab.com/snight/tick.git
```

# 二、配置TICK
## 1、配置InfluxDB数据库
用于存储telegraf收集上来的各种数据信息。

- 数据库API端口：8086
- 数据库ADMIN端口：8083
- 数据库名称：opsmonitor
- 数据库用户：root
- 数据库密码：pass
- 存储类: nfs-storage
- 持久化存储卷：30Gi

## 2、配置Telegraf代理程序
基于插件驱动的telegraf代理程序，用于定义各节点需要收集的信息类型，并上传至服务端。

- 数据库URL地址： [“http://influxdb-service:8086”]
- 数据库名称：opsmonitor
- 数据库用户：root
- 数据库密码：pass
- 不需要存储卷

## 3、安装Telegraf服务端
用于收集telegraf客户端上传来的各种数据，并写入InfluxDB数据库。

- 服务端口：8092/8186
- 数据库URL地址： [“http://influxdb-service:8086”]
- 数据库名称：opsmonitor
- 数据库用户：root
- 数据库密码：pass
- 不需要存储卷

## 4、配置Kapacitor
用来处理、监控和警告时间序列数据。

- API端口：9092
- 数据库URL地址： [“http://influxdb-service:8086”]
- 存储类: nfs-storage
- 持久化存储卷：5Gi

## 5、部署Chronograf
开源的度量分析和可视化工具

- HTTP端口：80
- NodePort端口：30005
- 存储类: nfs-storage
- 持久化存储卷：5Gi

## 6、创建Grafana
开源的度量分析和可视化工具

- HTTP端口：3000
- NodePort端口：30006
- 存储类: nfs-storage
- 持久化存储卷：5Gi

# 三、部署TICK和Grafana
```shell
kubectl create ns tick
kubectl create -f tick/influxdb.yaml
kubectl create -f tick/telegraf-ds.yaml
kubectl create -f tick/telegraf-infra.yaml
kubectl create -f tick/kapacitor.yaml
kubectl create -f tick/chronograf.yaml
kubectl create -f tick/grafana.yaml
```
